/// Library for controlling NeoHub heating controls and plugs from Heatmiser.
library neohub;

import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'dart:typed_data';

/// Abstract Sender type made abstract for testing.
abstract class Sender {
  Future<Map> send(Map message);
}

/// Send commands by logging them for testing
class TestingSender extends Sender {
  final List<Map> log = [];
  final Map response;

  TestingSender(this.response);

  @override
  Future<Map> send(Map message) {
    log.add(message);
    return Future.value(response);
  }
}

/// Send commands to a live server
class SocketSender extends Sender {
  final host;
  final port = 4242;
  final _terminator = [0, 13];

  Socket _sock;

  SocketSender(this.host);

  /// Send the given command [message].
  @override
  Future<Map> send(Map message) async {
    _sock = await Socket.connect(host, port);
    _sock.add(utf8.encode(jsonEncode(message)));
    _sock.add(_terminator);
    final ds = List<int>();
    await for (Uint8List event in _sock) {
      ds.addAll(event);
      if (event[event.length - 1] == 0) {
        break;
      }
    }
    // Don't forget to remove the stupid terminator
    ds.remove(0);
    final resp = jsonDecode(utf8.decode(ds));
    await _sock.close();
    return resp;
  }
}

/// The NeoHub API
class NeoHub {
  final Sender _sender;

  NeoHub.forSender(this._sender);

  NeoHub(String host) : _sender = SocketSender(host);

  /// GET_LIVE_DATA raw command.
  Future<Map> getRawLiveData() {
    final msg = {'GET_LIVE_DATA': 0};
    return _sender.send(msg);
  }

  /// GET_PROFILES raw command.
  Future<Map> getRawProfiles() {
    final msg = {'GET_PROFILES': 0};
    return _sender.send(msg);
  }

  /// Generate the Live data object hierarchy.
  Future<LiveData> getLiveData() async {
    final rawProfiles = await getRawProfiles();
    final rawLiveData = await getRawLiveData();
    return LiveData.fromLiveData(rawLiveData, rawProfiles);
  }

  /// GET_ZONES command.
  Future<Map> getZones() {
    final msg = {'GET_ZONES': 0};
    return _sender.send(msg);
  }
}

/// The read-only live data from the NeoHub
/// Note: I did not pick this terrible name, please don't judge me.
class LiveData {
  final List<Zone> zones = [];
  final Map<int, Profile> profiles = {};

  LiveData.fromLiveData(Map data, Map profilesData) {
    final devices = List<Map>.from(data['devices']);
    for (var k in profilesData.keys) {
      final profileData = profilesData[k];
      profiles[profileData['PROFILE_ID']] = Profile.fromLiveData(profileData);
    }
    for (var item in devices) {
      zones.add(Zone.fromLiveData(item, profiles));
    }
  }
}

/// A NeoHub profile according to Live Data
class Profile {
  final String name;
  final int profileId;

  Profile.fromLiveData(Map data)
      : name = data['name'],
        profileId = data['PROFILE_ID'];

  @override
  String toString() {
    return "Profile(name:$name, id:$profileId)";
  }
}

/// A NeoHub zone according to Live Data
class Zone {
  final name;
  final List<double> recentTemps = [];
  final bool isThermostat;
  final int profileId;
  final bool heatOn;
  final double actualTemp;
  Profile profile;

  Zone.fromLiveData(Map data, profiles)
      : name = data['ZONE_NAME'],
        isThermostat = data['THERMOSTAT'] ?? false,
        heatOn = data['HEAT_ON'],
        actualTemp = double.parse(data['ACTUAL_TEMP']),
        profileId = data['ACTIVE_PROFILE'] ?? -1 {
    final temps = List<String>.from(data['RECENT_TEMPS']);
    for (var item in temps) {
      recentTemps.add(double.parse(item ?? "0"));
    }
    profile = profileId > 0 ? profiles[profileId] : null;
  }

  @override
  String toString() {
    return "Zone(name:$name,profile:${profile?.name},isThermostat:$isThermostat,heatOn:$heatOn,temp:$actualTemp)";
  }
}
