/// Example of running the NeoHub
/// Change the Address to the IP address of your hub.

import 'package:neohub/neohub.dart';

// The address of your NeoHub
const NEOHUB_ADDRESS = '10.0.0.30';

main() async {
  final hub = NeoHub(NEOHUB_ADDRESS);
  final livedata = await hub.getLiveData();
  for (var z in livedata.zones) {
    print(z);
  }
}
