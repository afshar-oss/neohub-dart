import 'package:flutter/material.dart';
import 'package:neohub/neohub.dart';

import 'state.dart';

class ZoneListTile extends StatelessWidget {
  final Zone zone;

  ZoneListTile(this.zone);

  String get title {
    return '${zone.name}';
  }

  String get temperature {
    return '${zone.recentTemps[zone.recentTemps.length - 1]}°C';
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        //height: 50,
        //color: Colors.amber[colorCodes[index]],
        child: ListTile(
      title: Text(title),
      subtitle: Text(zone.profile?.name ?? ''),
      //trailing: zone.heatOn ? Icon(Icons.fireplace) : null
      trailing:
          Text(temperature, style: TextStyle(fontWeight: FontWeight.bold)),
      dense: true,
    ));
  }
}

class ZoneList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<LiveData>(
        valueListenable: NeoState.of(context).liveData,
        builder: (context, v, child) {
          if (v == null) {
            return Text('no data yet');
          } else {
            return ListView.builder(
                padding: const EdgeInsets.all(4),
                itemCount: v.zones.length,
                itemBuilder: (BuildContext context, int index) {
                  final Zone zone = v.zones[index];
                  return ZoneListTile(zone);
                });
          }
        });
  }
}

class HeatHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: ValueListenableBuilder(
          builder: (context, value, child) => Text(value),
          valueListenable: NeoState.of(context).ui.title,
        ),
      ),
      body: Container(
        child: Container(
          child: ZoneList(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => null,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
