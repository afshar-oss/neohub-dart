import 'package:flutter/material.dart';

import 'ui.dart';
import 'state.dart';

void main() {
  runApp(NeoBubble());
}

class NeoBubble extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: NeoState(child: HeatHome()),
    );
  }
}
