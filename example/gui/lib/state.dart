import 'package:flutter/material.dart';
import 'package:neohub/neohub.dart';
import 'package:tickvaluenotifier/tickvaluenotifier.dart';

final hub = NeoHub('10.0.0.30');

class UIState {
  final page = ValueNotifier<int>(0);
  final title = ValueNotifier<String>('NeoBubble');
}

class NeoState extends InheritedWidget {
  final ui = UIState();

  final liveData =
      TickValueNotifier<LiveData>(null, hub.getLiveData, Duration(seconds: 5));

  NeoState({Key key, Widget child}) : super(key: key, child: child);

  static NeoState of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<NeoState>();
  }

  bool updateShouldNotify(NeoState old) => true;
}
