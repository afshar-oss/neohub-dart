## [0.0.1] - 2020-10-06

* Initial working library.

## [0.0.2] - 2020-10-06

* Longer description in pubspec to get more points on pub.dev.

## [0.0.3] - 2020-10-07

* Create object hierarchies for live data.

## [0.0.4] - 2020-10-09

* Give the object hierarchies sane names.
