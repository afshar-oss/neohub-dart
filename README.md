# neohub

Dart / Flutter API for controlling
[NeoHub](https://www.heatmiser.com/en/neohub-smart-control/) from
[Heatmiser](https://www.heatmiser.com/).

Note: The Heatmiser API is not HTTP, it uses sockets so this likely won't work
from the web.

## Getting Started

For example:

```dart
/// Example of running the NeoHub
/// Change the Address to the IP address of your hub.

import 'package:neohub/neohub.dart';

// The address of your NeoHub
const NEOHUB_ADDRESS = '10.0.0.30';

main() async {
  final hub = NeoHub(NEOHUB_ADDRESS);
  final livedata = await hub.getLiveData();
  for (var z in livedata.zones) {
    print(z);
  }
}
```

This is from
[console/neohub_example.dart](https://gitlab.com/virchow-personal/neohub-dart/example/console/neohub_example.dart). There is a 
[GUI](https://gitlab.com/virchow-personal/neohub-dart/example/console/neohub_example.dart) example too in Flutter in the repository which has some widgets you can borrow.

## Reference

Not that useful, but [API Reference from Heatmiser](https://gitlab.com/virchow-personal/neohub-dart/-/blob/master/doc/Neohub_Api_For_Systems_Developers.pdf).
Email support@heatmiser.com if you need your own copy.
