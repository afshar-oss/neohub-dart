import 'package:flutter_test/flutter_test.dart';

import 'package:neohub/neohub.dart';

void main() {
  test('Test some commands.', () async {
    final sender = TestingSender({'Bedroom 2': 9, 'Bedroom 2 Ensuite': 10});
    final hub = NeoHub.forSender(sender);
    final resp = await hub.getZones();
    expect(resp, {'Bedroom 2': 9, 'Bedroom 2 Ensuite': 10});
    expect(sender.log[0], {'GET_ZONES': 0});
  });
}
